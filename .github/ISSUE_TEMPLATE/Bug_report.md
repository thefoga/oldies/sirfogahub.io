---
name: Bug report
about: Create a report to help me improve

---

Summary.

## Expected Result

What you expected.

## Actual Result

What happened instead.

## Reproduction Steps

Steps to reproduce the issue

## System Information

OS type, model ...