<div align="center">
	<h1>sirfoga.github.io</h1>

<a href="https://github.com/sirfoga/sirfoga.github.io/pulls"><img src="https://badges.frapsoft.com/os/v1/open-source.svg?v=103"></a> <a href="https://github.com/sirfoga/sirfoga.github.io/issues"><img src="https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat"></a> <a href="https://opensource.org/licenses/MIT"><img src="https://img.shields.io/badge/License-MIT-blue.svg"></a> <a href="https://saythanks.io/to/sirfoga"><img src="https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg" alt="Say Thanks!" /></a>
</div>

A Github Pages template for academic websites. This was forked (then detached) by [Stefano Fogarollo](https://github.com/sirfoga) from the [academicpages.github.io](https://github.com/academicpages/academicpages.github.io), which is © 2016 Michael Rose and released under the MIT License. See [LICENSE](LICENSE).

### Note: if you are using this repo and now get a notification about a security vulnerability, delete the Gemfile.lock file. 


## To run locally (not on GitHub Pages, to serve on your own computer)

0. Clone the repository and made updates as detailed above
0. Make sure you have ruby-dev, bundler, and nodejs installed: `apt install ruby-dev ruby-bundler nodejs`
0. Run `bundle clean` to clean up the directory (no need to run `--force`)
0. Run `bundle install` to install ruby dependencies. If you get errors, delete Gemfile.lock and try again.
0. Run `bundle exec jekyll liveserve` to generate the HTML and serve it from `localhost:4000` the local server will automatically rebuild and refresh the pages on change.


## Got questions?

If you have questions or general suggestions, don't hesitate to submit
a new [Github issue](https://github.com/sirfoga/sirfoga.github.io/issues/new).


## Contributing and feedback
<a href="https://github.com/sirfoga/sirfoga.github.io/issues"><img alt="Contributions welcome" src="https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat"></a>
<a href="https://opensource.org/licenses/MIT"><img alt="Open Source Love" src="https://badges.frapsoft.com/os/v1/open-source.svg?v=103"></a>

0. [open an issue](https://github.com/sirfoga/sirfoga.github.io/issues/new)
0. [fork](https://github.com/sirfoga/sirfoga.github.io/fork) this repository
0. create your feature branch (`git checkout -b my-new-feature`)
0. commit your changes (`git commit -am 'Added my new feature'`)
0. publish the branch (`git push origin my-new-feature`)
0. [open a PR](https://github.com/sirfoga/sirfoga.github.io/compare)

Suggestions and improvements are [welcome](https://github.com/sirfoga/sirfoga.github.io/issues)!


## Authors

| [![sirfoga](https://avatars0.githubusercontent.com/u/14162628?s=128&v=4)](https://github.com/sirfoga "Follow @sirfoga on Github") |
|---|
| [Stefano Fogarollo](https://sirfoga.github.io) |


## License
[MIT License](https://opensource.org/licenses/MIT)
